package test.service;

import entity.Todolist;
import repository.TodolistRepository;
import repository.TodolistRepositoryImpl;
import service.TodolistService;
import service.TodolistServiceImpl;

public class TodoListServiceTest {

    public static void main(String[] args) {

//        testShowTodoList();
//        testAddTodoList();
        testRemoveTodoList();

    }

    public static void testShowTodoList(){

        TodolistRepositoryImpl todolistRepository = new TodolistRepositoryImpl();
        todolistRepository.data[0] = new Todolist("Belajar Java Dasar");
        todolistRepository.data[1] = new Todolist("Belajar Java OOP");
        todolistRepository.data[2] = new Todolist("Belajar Java Standar Classes");
        todolistRepository.data[3] = new Todolist("Belajar Java Clean Code");
        TodolistService todolistService = new TodolistServiceImpl(todolistRepository);
        todolistService.showTodolist();

    }

    public static void testAddTodoList(){

        TodolistRepository todolistRepository = new TodolistRepositoryImpl();
        TodolistService todolistService = new TodolistServiceImpl(todolistRepository);

        todolistService.addTodolist("Belajar Java Dasar");
        todolistService.addTodolist("Belajar Java OOP");
        todolistService.addTodolist("Belajar Java Standar Classes");
        todolistService.addTodolist("Belajar Java Clean Code");

        testShowTodoList();
    }

    public static void testRemoveTodoList(){
        TodolistRepository todolistRepository = new TodolistRepositoryImpl();
        TodolistService todolistService = new TodolistServiceImpl(todolistRepository);

        todolistService.addTodolist("Belajar Java Dasar");
        todolistService.addTodolist("Belajar Java OOP");
        todolistService.addTodolist("Belajar Java Standar Classes");
        todolistService.addTodolist("Belajar Java Clean Code");

        todolistService.showTodolist();

        todolistService.removeTodolist(5);
        todolistService.removeTodolist(2);

        todolistService.showTodolist();
        todolistService.removeTodolist(2);
        todolistService.showTodolist();
        todolistService.removeTodolist(2);
        todolistService.showTodolist();
        todolistService.removeTodolist(1);
        todolistService.showTodolist();

    }
}
